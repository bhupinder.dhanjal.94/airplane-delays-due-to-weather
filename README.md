# Airplane Delays due to weather

This project includes the code that predicts whether flights will delay or not.
It contain data derived from 60 different files taken in period of 5 years.

This project file includes the code that extract the data from 60 different zip files than read all the files as one dataset.

It also include feature engineering, link to tableau public account that describe data visualized graphs.
